//
//  ColorExtension.swift
//  CrateClubNew
//
//  Created by MacPro on 28/01/2019.
//  Copyright © 2019 MacPro. All rights reserved.
//

import UIKit

extension UIColor {
    
    static func  colorActive() -> UIColor{
        return #colorLiteral(red: 0.3960784314, green: 0.4431372549, blue: 0.2117647059, alpha: 1)
    }
    
}
