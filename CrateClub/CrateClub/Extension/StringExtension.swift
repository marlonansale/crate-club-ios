//
//  StringExtension.swift
//  CrateClub
//
//  Created by MacPro on 09/01/2019.
//  Copyright © 2019 MacPro. All rights reserved.
//

import UIKit

extension String {
    
    init?(htmlEncodedString: String) {
        
        guard let data = htmlEncodedString.data(using: .utf8) else {
            return nil
        }
        
        
        let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
            NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html,
            NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue
        ]
        
        
        guard let attributedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil) else {
            return nil
        }
        
        self.init(attributedString.string)
    }
    
    
    
    
    func isValidEmail() -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: self)
    }
    
    public func cleanString() -> String! {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="
        
        var allowedCharacterSet = CharacterSet.urlQueryAllowed
        allowedCharacterSet.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        
        return addingPercentEncoding(withAllowedCharacters: allowedCharacterSet)
    }
    
    public func htmlReady() -> String! {
        var mainContent = self.replacingOccurrences(of: "font-family", with: "")
        
        mainContent = mainContent.replacingOccurrences(of: "font-size", with: "")
        mainContent = mainContent.replacingOccurrences(of: "width:", with: "")
        mainContent = mainContent.replacingOccurrences(of: "width=", with: "")
        mainContent = mainContent.replacingOccurrences(of: "height:", with: "")
        mainContent = mainContent.replacingOccurrences(of: "height=", with: "")
        mainContent = mainContent.replacingOccurrences(of: "sizes=", with: "")
        mainContent = mainContent.replacingOccurrences(of: "srcset=", with: "")
        mainContent = mainContent.replacingOccurrences(of: "id=", with: "")
        mainContent = mainContent.replacingOccurrences(of: "data-orig-size=", with: "")
        
        
        let kCSS = "content"
        
        return "<!DOCTYPE html>" +
            "<head><link rel=\"stylesheet\" type=\"text/css\" href=\"\(kCSS).css\">" +
            " <meta charset=\"utf-8\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0\" />" +
            "</head>" +
            "<body style='background-color: transparent; color:black;' >" +
        "<div style='background-color: transparent; height:auto;   overflow: scroll;' id='divContainer'>\(mainContent)</div></body></html"
        
    }
    
    var isNumeric: Bool {
        guard self.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        return Set(self).isSubset(of: nums)
    }
    
    var htmlToAttributedString: NSMutableAttributedString? {
        guard let data = data(using: .utf8) else { return  NSMutableAttributedString() }
        do {
            return try NSMutableAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSMutableAttributedString()
        }
    }
    
    
    
    var stripped: String {
        
        let str = self.replacingOccurrences(of: "Â", with: " ")
        let okayChars = Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-=().!_:@#$|'/")
        return str.filter {okayChars.contains($0) }
    }
    
    
    static func cleanValue(obj:Any) -> String {
        if let b = obj as? String {
            
            
            return b == "<null>" ? "" : b
            
        }else if let b = obj as? Double{
            
            return  String(format: "%.0f", b)
            
        } else{
            print("Error") // Was not a string
            
            
            return String(describing:obj)
        }
    }
    
}
