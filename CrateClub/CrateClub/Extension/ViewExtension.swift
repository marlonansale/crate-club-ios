//
//  ViewExtension.swift
//  CrateClubNew
//
//  Created by MacPro on 28/01/2019.
//  Copyright © 2019 MacPro. All rights reserved.
//

import UIKit

extension UIView {
    
    func setRadius()  {
        let path = UIBezierPath(roundedRect:self.bounds,
                                byRoundingCorners:[.topLeft, .bottomLeft],
                                cornerRadii: CGSize(width: 3, height:  3))
        
        let maskLayer = CAShapeLayer()
        
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }
    
    
    func bindFrameToSuperviewBounds() {
        guard let superview = self.superview else {
            print("Error! `superview` was nil – call `addSubview(view: UIView)` before calling `bindFrameToSuperviewBounds()` to fix this.")
            return
        }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        superview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat:"H:|-0-[subview]-0-|" , options: .directionLeadingToTrailing, metrics: nil, views: ["subview": self]))
        superview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat:"V:|-0-[subview]-0-|" , options: .directionLeadingToTrailing, metrics: nil, views: ["subview": self]))
        
    }
    
}
