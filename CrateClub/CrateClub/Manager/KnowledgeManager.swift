//
//  KnowledgeManager.swift
//  CrateClub
//
//  Created by MacPro on 09/01/2019.
//  Copyright © 2019 MacPro. All rights reserved.
//

import Foundation

final class KnowledgeManager: RootManager {
    static let shared = KnowledgeManager()
    
    
    func getKnowledge(page:Int, complete:@escaping CompletionHandler, fail:@escaping FailHandler) {
        let urlString = URL(string: URL_CRATE_CLUB+"/posts?page=\(page)&per_page=\(PER_PAGE)")
        
        self.urlTask = URLSession.shared.dataTask(with: urlString!) { (data, response, error) in
            
            if let httpResponse = response as? HTTPURLResponse{
                if (httpResponse.statusCode == 200) {
                    if error != nil {
                        fail(error!.localizedDescription)
                    } else {
                        
                        do {
                            if let array = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSArray{
                                var arr = [ArticleModel]()
                                
                                for dict in array {
                                    
                                    arr.append(ArticleModel.init(dict: dict as! Dictionary<String, Any>))
                                }
                                
                                complete(arr)
                            }else{
                                fail("Something went wrong")
                            }
                            
                        } catch {
                            
                            fail("Something went wrong")
                            
                            
                        }
                        
                    }
                }else{
                    fail("Something went wrong")
                }
                
            }else{
                fail("Something went wrong")
            }
            
        }
        self.urlTask?.resume()
        
    }

    
}
