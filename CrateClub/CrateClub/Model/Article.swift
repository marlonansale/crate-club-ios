//
//  Article.swift
//  CrateClub
//
//  Created by MacPro on 09/01/2019.
//  Copyright © 2019 MacPro. All rights reserved.
//

import Foundation
import CoreData

struct Article {
    
    var content:String?
    var excerpt:String?
    var title:String?
    var link:URL?
    var caption:String?
    
    var category:String?
    var categoryHolder:String?
    var categoryID:Int = 0
    
    var author:String?
    var authorImage:URL?
    var authorDescription:String?
    var authorID:Int = 0
    var authorCred:String?
    
    var imgThumb:URL?
    var imgCat:URL?
    var imgPost:URL?
    var imgFull:URL?
    
    var dateString:String?
    var date:Date?
    var articleID:Int = 0
    var type:String?
    
   
    
    
    //var articles:[ArticleModel]?
    
    
    
    init(dict:Dictionary<String,Any>) {
        
        //print("dict \(dict)")
        
        self.articleID = dict["id"] as? Int ?? 0
        
        if let titleDict = dict["title"] as? Dictionary<String,Any> {
            self.title  = titleDict["rendered"] as? String ?? ""
            self.title = String(htmlEncodedString: self.title ?? "")
        }
        
        if let contentDict = dict["content"] as? Dictionary<String,Any> {
            self.content  = contentDict["rendered"] as? String ?? ""
        }
        
        if let excerptDict =  dict["excerpt"] as? Dictionary<String,Any> {
            
            
            
            self.excerpt  = excerptDict["rendered"] as? String ?? ""
            self.excerpt = String(htmlEncodedString: self.excerpt ?? "")
        }
        
        let strLink = dict["link"] as? String ?? URL_CRATE_CLUB
        self.link  = URL.init(string: strLink)
        
        self.type = dict["type"] as? String ?? "news"
        
        if let imgDict = dict["featured_images"] as? Dictionary<String,Any> {
            
            let strImgThumb  = imgDict["post-thumbnail"] as? String ?? ""
            let strImgCat  = imgDict["cat-feat-img"] as? String ?? ""
            //let strImgPost  = imgDict["wp_rp_thumbnail"] as? String ?? ""
            let strImgFull  = imgDict["full"] as? String ?? ""
            
            
            
            self.imgThumb = URL.init(string: strImgThumb)
            self.imgCat = URL.init(string: strImgCat)
            //self.imgPost = URL.init(string: strImgPost)
            self.imgFull = URL.init(string: strImgFull)
            //             print("strImgPost \(strImgPost)")
            //             print("strImgPost \(self.imgPost)")
            
            self.caption = imgDict["image_caption"] as? String ?? ""
            
        }
        
        let strDate = dict["date"] as? String ?? nil
        
        if strDate != nil {
            let dateFormat = DateFormatter()
            dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            self.date = dateFormat.date(from:strDate!)
            
        }
        
        
      
        
        if let authorDict =  dict["author_details"] as? Dictionary<String,Any> {
            self.authorID = dict["author"] as! Int
            self.author  = authorDict["author_name"] as? String ?? ""
            self.authorDescription = authorDict["author_description"] as? String ?? ""
            
            let strImgThumb  = authorDict["author_avatar"] as? String ?? ""
            self.authorImage = URL.init(string: strImgThumb)
            
            self.authorCred = authorDict["author_creds"] as? String
        }
        
        if let dictCat = dict["category_details"] as? Dictionary<String,Any> {
            self.category  = dictCat["category_name"] as? String
            
            if let arrCat = dict["categories"] as? [Int]{
                if arrCat.count > 0{
                    self.categoryID = arrCat[0]
                }
            }
        }
          
        
        
    }
    
    
    
    
    
}
