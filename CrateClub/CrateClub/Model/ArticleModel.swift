//
//  ArticleModel.swift
//  CrateClub
//
//  Created by MacPro on 09/01/2019.
//  Copyright © 2019 MacPro. All rights reserved.
//

import Foundation

struct ArticleModel{
    
    var content:String?
    var excerpt:String?
    var title:String?
    var link:URL?
    var author:String?
    var authorImage:URL?
    var imgCat:URL?
    var imgPost:URL?
    var imgFull:URL?
    var date:Date?
    
    init(dict:Dictionary<String,Any>) {
        
        
        let dictTitle = dict["title"] as! Dictionary<String,String>
        self.title = String(htmlEncodedString: dictTitle["rendered"]!)
        
        let dictExcerpt = dict["excerpt"] as! Dictionary<String,Any>
        self.excerpt = dictExcerpt["rendered"] as? String
        
        let dictContent = dict["content"] as! Dictionary<String,Any>
        self.content = dictContent["rendered"] as? String
        
        self.link = URL.init(string: dict["link"] as! String)
        
        let dictAuthor = dict["author_details"] as! Dictionary<String,Any>
        
        if let strAuthor  = dictAuthor["author_name"] as? String {
            self.author = strAuthor
        }else{
            self.author = ""
        }
        
        self.authorImage = URL.init(string: dictAuthor["author_avatar"] as! String)
        
        let dictImage = dict["featured_images"] as! Dictionary<String,Any>
        self.imgCat = URL.init(string: dictImage["cat-feat-img"] as! String)
        self.imgPost = URL.init(string: dictImage["post-thumbnail"] as! String)
        self.imgFull = URL.init(string: dictImage["full"] as! String)
        
        
        if let strDate  = dict["date"]  as? String {
            let dateFormat = DateFormatter()
            dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            self.date = dateFormat.date(from: strDate)
        }else{
            self.date = Date()
        }
        
       
        //dateFormat.date(from:super.cleanValue(obj: dict["date"] ?? ""))
        
        self.excerpt = self.excerpt?.replacingOccurrences(of: "[&hellip;]", with: "<span class='readmore'>.. [READ MORE..]</span>")
        //self.excerpt = self.cleanContent(content: self.setDataExcerpt(knowledge: self))
        
    }
    
    func setDataExcerpt(knowledge:ArticleModel) -> String{
        
        //print("knowledge \(knowledge.excerpt!)")
        
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "MMMM, dd yyyy"
        
        var html = "<div class='header'><span class='authorHeader'>\(knowledge.author!) </span> <span class='dateHeader'>\(dateFormat.string(from: knowledge.date!))</span></div>"
        
        html = "\(html) \(knowledge.excerpt!)"
        
        return html
        
    }
    
    
}
