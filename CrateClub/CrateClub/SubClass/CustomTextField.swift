//
//  CustomTextField.swift
//  CrateClubNew
//
//  Created by MacPro on 17/01/2019.
//  Copyright © 2019 MacPro. All rights reserved.
//

import UIKit
import Material

class CustomTextField: ErrorTextField {
 
    override func draw(_ rect: CGRect) {
        
    }
    
    
    override func prepare() {
        super.prepare()
        self.detailVerticalOffset = 0
        self.detailLabel.layoutEdgeInsets.left = 20
        self.placeholderNormalColor = .white
        self.placeholderActiveColor = .white
        self.detailColor = .white
        self.dividerActiveHeight = 0
        self.dividerNormalHeight = 0
        self.textColor = .black
        self.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.18)
        self.textInset = 10
        
    }
    
}
