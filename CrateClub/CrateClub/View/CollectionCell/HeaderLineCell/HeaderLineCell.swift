//
//  HeaderLineCell.swift
//  CrateClub
//
//  Created by Marlon Ansale on 13/06/2017.
//  Copyright © 2017 rugged marlon. All rights reserved.
//

import UIKit

let HeaderLineCellHeight:CGFloat = 50

class HeaderLineCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
