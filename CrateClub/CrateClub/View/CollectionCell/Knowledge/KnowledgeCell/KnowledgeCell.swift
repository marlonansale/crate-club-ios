//
//  KnowledgeCell.swift
//  CrateClub
//
//  Created by Marlon Ansale on 13/06/2017.
//  Copyright © 2017 rugged marlon. All rights reserved.
//

import UIKit



class KnowledgeCell: UITableViewCell {
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var constantHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if UIDevice.current.userInterfaceIdiom == .pad ||  UIDevice.current.userInterfaceIdiom == .unspecified {
            constantHeight.constant = 300
            //imgView.contentMode = UIViewContentMode.scaleAspectFit
        }else {
             constantHeight.constant = 250
        }
        
        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

    
}
