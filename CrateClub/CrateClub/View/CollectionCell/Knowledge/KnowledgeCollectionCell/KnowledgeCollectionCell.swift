//
//  KnowledgeCollectionCell.swift
//  CrateClub
//
//  Created by Marlon Ansale on 30/11/2017.
//  Copyright © 2017 rugged marlon. All rights reserved.
//

import UIKit
import Material

let KnowledgeCellHeight:CGFloat = UIDevice.current.userInterfaceIdiom == .unspecified || UIDevice.current.userInterfaceIdiom == .pad ? 530 : 430

class KnowledgeCollectionCell: CollectionViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var constantHeight: NSLayoutConstraint!
    
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    
//    override class var layerClass: AnyClass {
//        return MDCShadowLayer.self
//    }
//
//    var shadowLayer: MDCShadowLayer {
//        return self.layer as! MDCShadowLayer
//    }
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if UIDevice.current.userInterfaceIdiom == .pad ||  UIDevice.current.userInterfaceIdiom == .unspecified {
            constantHeight.constant = 350
            
        }else {
            constantHeight.constant = 250
        }
        
    }
    
    

    func setData(article:ArticleModel){
        
        self.lblTitle.text = article.title
        self.imgView.sd_setImage(with: article.imgPost, placeholderImage: UIImage.init(named: "placeholder"), options: .cacheMemoryOnly)
        self.lblDescription.text = article.excerpt
    }

}
