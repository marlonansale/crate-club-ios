//
//  ProfileCollectionCell.swift
//  CrateClub
//
//  Created by Marlon Ansale on 26/07/2017.
//  Copyright © 2017 rugged marlon. All rights reserved.
//

import UIKit
import RSFloatInputView

let ProfileCollectionCell_Height:CGFloat = 260

class ProfileCollectionCell: UICollectionViewCell {
    @IBOutlet weak var viewFname: RSFloatInputView!
    @IBOutlet weak var viewSname: RSFloatInputView!
    @IBOutlet weak var viewEmail: RSFloatInputView!
    @IBOutlet weak var btnUpdate: CustomButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//
//        viewFname.textField.delegate = self
//        viewSname.textField.delegate = self
//        viewEmail.textField.delegate = self
        
        viewEmail.textField.keyboardType = UIKeyboardType.emailAddress
        
        viewEmail.textField.autocorrectionType = .no
        viewFname.textField.autocorrectionType = .no
        viewSname.textField.autocorrectionType = .no
        
        viewFname.textField.tag = 1
        viewSname.textField.tag = 2
        viewEmail.textField.tag = 3

        
    }
    
    func setData(user:UserModel){
        
        
//        viewFname.textField.text = user.firstName
//        viewSname.textField.text = user.lastName
//        viewEmail.textField.text = user.email
        
        
        viewFname.changeToFloat(animated: true)
        viewSname.changeToFloat(animated: true)
        viewEmail.changeToFloat(animated: true)
        
        
    }

}
