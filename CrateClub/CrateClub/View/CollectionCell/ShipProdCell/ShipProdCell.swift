//
//  ShipProdCell.swift
//  CrateClub
//
//  Created by Marlon Ansale on 28/11/2017.
//  Copyright © 2017 rugged marlon. All rights reserved.
//

import UIKit

let ShipProdCellHeight:CGFloat = 100

class ShipProdCell: UITableViewCell {
    @IBOutlet weak var viewProd: UIView!
    @IBOutlet weak var lblProduct: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewProd.layer.borderWidth = 5
        viewProd.layer.borderColor = UIColor.black.cgColor
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
