//
//  ShipProdHorizontalCollectionCell.swift
//  CrateClub
//
//  Created by Marlon Ansale on 29/11/2017.
//  Copyright © 2017 rugged marlon. All rights reserved.
//

import UIKit
import Material

class ShipProdHorizontalCollectionCell: UICollectionViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var webViewDetail: UIWebView!
    
    
    @IBOutlet weak var btnMonth: CustomButton!
    @IBOutlet weak var btnSemiAnnual: CustomButton!
    @IBOutlet weak var btnAnnual: CustomButton!
    
    @IBOutlet weak var btnGiftMonth: CustomButton!
    @IBOutlet weak var btnGiftSemiAnnual: CustomButton!
    @IBOutlet weak var btnGiftAnnual: CustomButton!
    
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblSemiAnnual: UILabel!
    @IBOutlet weak var lblAnnual: UILabel!
    
    @IBOutlet weak var lblGiftMonth: UILabel!
    @IBOutlet weak var lblGiftSemiAnnual: UILabel!
    @IBOutlet weak var lblGiftAnnual: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        webViewDetail.backgroundColor = UIColor.clear;
        webViewDetail.isOpaque = false
    }
    
    func setData(product:ProductDataModel){
     
//        self.lblTitle.text = product.product
//        self.imgView.sd_setImage(with: product.image, completed: nil)
//
//        self.webViewDetail.loadHTMLString(product.productDescription!, baseURL: NSURL.fileURL(withPath: Bundle.main.path(forResource: kCSS, ofType: "css")!))
//
//        if product.arrTerm!.count > 2 {
//            btnMonth.isHidden = false
//            lblMonth.isHidden = false
//
//
//            let month = product.arrTerm![2]
//            let semiAnnual = product.arrTerm![1]
//            let annual = product.arrTerm![0]
//
//            btnMonth.setTitle(month.buyButton, for: .normal)
//            btnSemiAnnual.setTitle(semiAnnual.buyButton, for: .normal)
//            btnAnnual.setTitle(annual.buyButton, for: .normal)
//
//            lblMonth.text = month.subText
//            lblSemiAnnual.text = semiAnnual.subText
//            lblAnnual.text = annual.subText
//
//        }else{
//            btnMonth.isHidden = true
//            lblMonth.isHidden = true
//
//            let semiAnnual = product.arrTerm![1]
//            let annual = product.arrTerm![0]
//
//            btnSemiAnnual.setTitle(semiAnnual.buyButton, for: .normal)
//            btnAnnual.setTitle(annual.buyButton, for: .normal)
//
//            lblSemiAnnual.text = semiAnnual.subText
//            lblAnnual.text = annual.subText
//
//        }
        
        
        
    }

}
