//
//  ShipmentCollectionCell.swift
//  CrateClub
//
//  Created by Marlon Ansale on 13/07/2017.
//  Copyright © 2017 rugged marlon. All rights reserved.
//

import UIKit
import RSFloatInputView
import Material

let ShipmentCancelled_Height:CGFloat = 385.0
let ShipmentShipped_Height:CGFloat = 543.0

class ShipmentCollectionCell: CollectionViewCell {

    @IBOutlet weak var rfProducts: RSFloatInputView!
    @IBOutlet weak var rfTracking: RSFloatInputView!
    @IBOutlet weak var rfOrderDate: RSFloatInputView!

    @IBOutlet weak var rfOrder: RSFloatInputView!
    @IBOutlet weak var rfSubscription: RSFloatInputView!
    @IBOutlet weak var rfShipment: RSFloatInputView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var txtViewAddress: UITextView!
    @IBOutlet weak var btnTracking:UIButton!
    
    @IBOutlet weak var btnCrates: CustomButton!
    @IBOutlet weak var rfShippedDate: RSFloatInputView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        rfProducts.textField.isUserInteractionEnabled = false
        rfOrderDate.textField.isUserInteractionEnabled = false
        rfTracking.textField.isUserInteractionEnabled = false
        rfOrder.textField.isUserInteractionEnabled = false
        rfSubscription.textField.isUserInteractionEnabled = false
        rfShipment.textField.isUserInteractionEnabled = false
        txtViewAddress.textContainerInset = UIEdgeInsets(top: 25, left: 10, bottom: 0, right: 10)
        self.rfShippedDate.textField.isUserInteractionEnabled = false
        
    }
    

    func setData(shipment:ShipmentModel, checkData:Bool)  {
        
        rfProducts.changeToFloat(animated: true)
        rfOrderDate.changeToFloat(animated: true)
        rfTracking.changeToFloat(animated: true)
        rfOrder.changeToFloat(animated: true)
        rfSubscription.changeToFloat(animated: true)
        rfShipment.changeToFloat(animated: true)
        rfShippedDate.changeToFloat(animated: true)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd yyyy"
        
        
//        rfProducts.textField.text = shipment.product
//        rfTracking.textField.text = shipment.trackingNumber
//        rfOrderDate.textField.text = dateFormatter.string(from: shipment.shipmentDate! as Date)
//        rfOrder.textField.text =  shipment.orderid!
//        rfSubscription.textField.text = shipment.subscriptionid!
//        rfShipment.textField.text = shipment.shipmentid
//
//        txtViewAddress.text = shipment.address
//
//
//        self.lblStatus.text = shipment.shipmentStatus?.uppercased()
//
//        if shipment.shipmentStatus == "shipped" {
//            self.lblStatus.backgroundColor = self.hexStringToUIColor(hex: "657136")
//        }else {
//            self.lblStatus.backgroundColor = self.hexStringToUIColor(hex: "D81F27")
//        }
//
//
//        if checkData
//        {
//
//            self.rfShippedDate.textField.text = dateFormatter.string(from: (shipment.shippedAt! as Date))
//            self.btnCrates.isHidden = false
//            self.rfShippedDate.isHidden = false
//
//        }else{
//
//            self.btnCrates.isHidden = true
//            self.rfShippedDate.isHidden = true
//
//        }
//
//
//        if shipment.trackingNumber == "N/A" {
//            rfTracking.backgroundColor = UIColor(hex: "EBEBEB")
//            rfTracking.textField.textColor = UIColor.black
//            rfTracking.placeHolderLabel.foregroundColor = UIColor.darkGray.cgColor
//        }else{
//            rfTracking.backgroundColor = UIColor(hex:"938983")
//            rfTracking.textField.textColor = UIColor.white
//            rfTracking.placeHolderLabel.foregroundColor = UIColor.white.cgColor
//        }
        
        
        
        
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
