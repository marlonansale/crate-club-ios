//
//  SubscriptionCollectionCell.swift
//  CrateClub
//
//  Created by Marlon Ansale on 22/06/2017.
//  Copyright © 2017 rugged marlon. All rights reserved.
//

import UIKit
import RSFloatInputView

let SubscriptionCollectionCell_Height:CGFloat = 350

class SubscriptionCollectionCell: UICollectionViewCell {
    
    
    @IBOutlet weak var btnUpdate: CustomButton!
    @IBOutlet weak var rfReferrence: RSFloatInputView!
    @IBOutlet weak var rfStatus: RSFloatInputView!
    @IBOutlet weak var rfPrice: RSFloatInputView!
    @IBOutlet weak var rfTerm: RSFloatInputView!
    @IBOutlet weak var rfProduct: RSFloatInputView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        rfReferrence.textField.isUserInteractionEnabled = false
        rfStatus.textField.isUserInteractionEnabled = false
        rfPrice.textField.isUserInteractionEnabled = false
        rfTerm.textField.isUserInteractionEnabled = false
        rfProduct.textField.isUserInteractionEnabled = false
        rfStatus.isUserInteractionEnabled = false
        
    }
    
    func setData(subscription:SubscriptionModel)  {
        
        rfReferrence.changeToFloat(animated: true)
        rfStatus.changeToFloat(animated: true)
        rfPrice.changeToFloat(animated: true)
        rfTerm.changeToFloat(animated: true)
        rfProduct.changeToFloat(animated: true)
        
        
//        rfProduct.textField.text = subscription.product
//        rfStatus.textField.text = subscription.status?.uppercased()
//        rfPrice.textField.text = subscription.price
//        rfTerm.textField.text = subscription.term
//
//        if subscription.subscriptionid != nil {
//            rfReferrence.textField.text = "#\(subscription.subscriptionid!)"
//        }
//
//
//
//        if subscription.status?.uppercased() == "ACTIVE" {
//
//            rfStatus.backgroundColor = UIColor(hex: kColorActive)
//            rfStatus.textField.textColor = UIColor.white
//            rfStatus.placeHolderLabel.foregroundColor = UIColor.white.cgColor
//
//        } else {
//
//            rfStatus.backgroundColor = UIColor(hex: kColorCancelled)
//            rfStatus.textField.textColor = UIColor.white
//            rfStatus.placeHolderLabel.foregroundColor = UIColor.white.cgColor
//
//        }
        
    }
    
}
