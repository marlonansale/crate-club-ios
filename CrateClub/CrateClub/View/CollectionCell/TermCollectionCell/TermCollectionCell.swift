//
//  TermCollectionCell.swift
//  CrateClub
//
//  Created by Marlon Ansale on 15/07/2017.
//  Copyright © 2017 rugged marlon. All rights reserved.
//

import UIKit

let TermCollectionCell_Height:CGFloat = 240
class TermCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewTerm: UIImageView!
    @IBOutlet weak var lblTerm: UILabel!
    @IBOutlet weak var lblTermPrice: UILabel!
    @IBOutlet weak var btnSubscribe: CustomButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(termModel:TermDataModel, isFirst:Bool){
        
//        self.lblTerm.text = termModel.term!
//
//        self.lblTermPrice.text = "$\(termModel.termPrice!)"
        
        if isFirst {
            self.imageViewTerm.image = UIImage.init(named: "term1")
        }else{
            self.imageViewTerm.image = UIImage.init(named: "term2")
        }
        
    }

}
