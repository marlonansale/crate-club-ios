//
//  UnderlineLabel.swift
//  CrateClubNew
//
//  Created by MacPro on 28/01/2019.
//  Copyright © 2019 MacPro. All rights reserved.
//

import UIKit

class UnderlineLabel: UILabel {
    
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        
        
        let border = CALayer()
        border.backgroundColor = UIColor.colorActive().cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - 3, width: frame.size.width, height: 3)
        self.layer.addSublayer(border)
        
        self.backgroundColor = .clear
        self.textColor = UIColor.colorActive()
    }
    
    
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}
