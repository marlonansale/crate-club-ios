//
//  CrateViewController.swift
//  CrateClubNew
//
//  Created by MacPro on 25/01/2019.
//  Copyright © 2019 MacPro. All rights reserved.
//

import UIKit

class CrateViewController: RootDeviceCheckerViewController {

    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var btnMonth: UIButton!
    @IBOutlet weak var btnPast: UIButton!
    @IBOutlet weak var constantPast: NSLayoutConstraint!
    @IBOutlet weak var constantMonth: NSLayoutConstraint!
    
    @IBOutlet weak var constantRight: NSLayoutConstraint!
    @IBOutlet weak var constantLeft: NSLayoutConstraint!
    
    @IBOutlet weak var viewEmpty: UIView!
    var intCurView = 1
    
    @IBOutlet weak var btnSubscribe: CustomButton!
    @IBOutlet weak var lblEmpty: UILabel!
    @IBOutlet weak var constantBoxTop: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
     // MARK: - Actions
    
    @IBAction func onUpcomming(_ sender: Any) {
        
    }
    
    @IBAction func onShipped(_ sender: Any) {
        
    }
    
    
    
}
