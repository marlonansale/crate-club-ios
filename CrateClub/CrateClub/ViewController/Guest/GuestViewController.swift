//
//  GuestViewController.swift
//  CrateClubNew
//
//  Created by MacPro on 17/01/2019.
//  Copyright © 2019 MacPro. All rights reserved.
//

import UIKit

class GuestViewController: RootViewController {
    
    @IBOutlet weak var constantWidth: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var cellView: UITableViewCell!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.isScrollEnabled = false
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            
            constantWidth.constant = 500
        }else{
            constantWidth.constant = 320
        }
    }
    
    // MARK: - Actions
    @IBAction func actionLearnMore(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeToTab()
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension GuestViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellView.frame.height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cellView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
}
