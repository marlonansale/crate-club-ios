//
//  LoginViewController.swift
//  CrateClub
//
//  Created by MacPro on 09/01/2019.
//  Copyright © 2019 MacPro. All rights reserved.
//

import UIKit
import Material

class LoginViewController: RootViewController {

    @IBOutlet var viewHeader: UIView!
    
 
    @IBOutlet var cellView: UITableViewCell!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var btnGuest: CustomButton!
    @IBOutlet weak var btnSubmit: CustomButton!
    
    @IBOutlet weak var lblAccount: UILabel!
    
    @IBOutlet weak var constantWidth: NSLayoutConstraint!
    @IBOutlet weak var constantHeight: NSLayoutConstraint!
    
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var txtPassword: CustomTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - ACTIONS
    
    @IBAction func actionSubmit(_ sender: Any) {

    }
    
    
    @IBAction func actionGuest(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeToTab()
    }
    
    @IBAction func actionClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func actionForgotPassword(_ sender: Any) {
        
    }
    
}

extension LoginViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return 140
        }else{
            
            return 100
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return viewHeader
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        cellView.clipsToBounds = true
        return cellView
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellView.frame.size.height
    }
    
   
}


extension LoginViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == "\n" {
            
            self.view.endEditing(true)
            
            if textField == txtEmail {
                txtPassword.becomeFirstResponder()
                
            }else if textField == txtPassword {
                //submit()
            }
        }
        
        
        return true
    }
}
