//
//  ContactViewController.swift
//  CrateClub
//
//  Created by MacPro on 09/01/2019.
//  Copyright © 2019 MacPro. All rights reserved.
//

import UIKit
import MessageUI

import ZendeskSDK
import ZendeskCoreSDK
import ZendeskProviderSDK

class ContactViewController: RootDeviceCheckerViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var cellView: UITableViewCell!
    @IBOutlet weak var btnContact: UIButton!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        view1.setRadius()
        view2.setRadius()
        view3.setRadius()
    }
    

    // MARK: - Actions
    @IBAction func actionChat(_ sender: Any) {
    
    }
    
    @IBAction func actionContactUs(_ sender: Any) {
    
    }
    
    @IBAction func actionFacebook(_ sender: Any) {
        UIApplication.shared.open(URL.init(string: "https://www.facebook.com/crateclubofficial/")!, options: [:], completionHandler: nil)
    }
    
    
    @IBAction func actionTwitter(_ sender: Any) {
        
        UIApplication.shared.open(URL.init(string: "https://twitter.com/sofrep")!, options: [:], completionHandler: nil)
    }
    
    @IBAction func actionInstagram(_ sender: Any) {
        
        UIApplication.shared.open(URL.init(string: "https://www.instagram.com/officialcrateclub/")!, options: [:], completionHandler: nil)
    }
    
    
    @IBAction func actionYoutube(_ sender: Any) {
        UIApplication.shared.open(URL.init(string: "https://www.youtube.com/channel/UCcOT370bAtA1nFXcwBA9GCA")!, options: [:], completionHandler: nil)
    }

}

extension ContactViewController : UITableViewDelegate,
UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return cellView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 490
        
    }
}
