//
//  TabBarController.swift
//  CrateClubNew
//
//  Created by MacPro on 25/01/2019.
//  Copyright © 2019 MacPro. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBar.barTintColor = .black
        self.tabBar.backgroundColor = .black
    }
}
