//
//  KnowledgeViewController.swift
//  CrateClubNew
//
//  Created by MacPro on 30/01/2019.
//  Copyright © 2019 MacPro. All rights reserved.
//

import UIKit
import WebKit

class KnowledgeViewController: RootDeviceCheckerViewController {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet var cellHeader: UITableViewCell!
    @IBOutlet var cellImage: UITableViewCell!
    @IBOutlet var cellDetails: UITableViewCell!
    @IBOutlet var cellShare: UITableViewCell!
    
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewWeb: UIView!
    var article:ArticleModel?
    var articleId:String?
    var heightContent:CGFloat = 1000
    var webViewContent:WKWebView = WKWebView.init()
    
    var hasLoad = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !self.hasLoad {
            //hudShow()
            
            if articleId != nil {
                
//                KnowledgeManager.shared.getKnowledge(articleId: self.articleId! , complete: { (complete) in
//
//                    if complete is ArticleModel {
//
//                        self.article = complete as? ArticleModel
//                        self.loadContent()
//
//                    }else{
//                        self.navigationController?.popViewController(animated: false)
//                        self.alertError(message: "Article not found")
//                    }
//
//                })
            }else{
                loadContent()
            }
            
        }
    }
    
    func  loadContent(){
        
        lblTitle.text = article?.title
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            imgView.sd_setImage(with: article?.imgFull, placeholderImage:#imageLiteral(resourceName: "placeholder"))
        }else{
            imgView.sd_setImage(with: article?.imgPost, placeholderImage:#imageLiteral(resourceName: "placeholder"))
        }
        
        
        
        let wkWebConfig = WKWebViewConfiguration.init()
        webViewContent = WKWebView.init(frame: .zero, configuration: wkWebConfig)
        
        webViewContent.scrollView.layer.masksToBounds = true
        webViewContent.navigationDelegate = self;
        
        self.webViewContent.scrollView.isScrollEnabled = false;
        
       
        
        
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "MMMM, dd yyyy"
        
//        var html = "<div class='headerContent'><span class='authorHeader'>\(article!.author!) </span> <span class='dateHeader'>\(dateFormat.string(from: article!.date!))</span></div>"
//
//        html = "\(html) \(article!.content!)"
        

        self.webViewContent.loadHTMLString(self.article!.content!.htmlReady(), baseURL: NSURL.fileURL(withPath: Bundle.main.path(forResource: kCSS, ofType: "css")!) )
        
        self.webViewContent.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        
        self.viewWeb.addSubview(self.webViewContent)
        self.webViewContent.bindFrameToSuperviewBounds()
        
        self.webViewContent.scrollView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        self.webViewContent.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        
        if (keyPath == "contentSize" && object is UIScrollView) {
            
            if (self.webViewContent.estimatedProgress == 1) {
                
                self.webViewContent.evaluateJavaScript("document.getElementById('divContainer').clientHeight", completionHandler: { (object, error) in
                    self.hasLoad = true
                    self.heightContent = object as! CGFloat
                    self.heightContent += 16
                    //self.hudDismiss()
                    self.tableView.reloadData()
                })
                
                
                self.webViewContent.scrollView.removeObserver(self, forKeyPath: "contentSize")
                
            }
        }
    }

    
    // MARK: ACTION
    
    @IBAction func actionShare(_ sender: UIButton) {
        let activityController = UIActivityViewController.init(activityItems: [self.article!.link!], applicationActivities: nil)
        
        if UIDevice.current.userInterfaceIdiom == .pad ||  UIDevice.current.userInterfaceIdiom == .unspecified {
            activityController.popoverPresentationController?.sourceView = sender;
            
        }
        
        self.present(activityController, animated: true) {
            
        }
    }
}


extension KnowledgeViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 4;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            return cellHeader
        } else if indexPath.row == 1 {
            return cellImage
        }else if indexPath.row == 2{
            return cellDetails
        }else{
            return cellShare
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return cellHeader.frame.size.height
        } else if indexPath.row == 1 {
            if UIDevice.current.userInterfaceIdiom == .pad {
                return 350
            }else{
                return 250
                
            }
            
        }else if indexPath.row == 2{
            return heightContent
        }else {
            return cellShare.frame.size.height
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let arrCell = self.tableView.visibleCells
        
        for cell in arrCell
        {
            if cell == self.cellDetails {
                self.webViewContent.setNeedsLayout()
            }
        }
    }
}


extension KnowledgeViewController : WKNavigationDelegate, UIScrollViewDelegate {
    
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return nil
    }
    
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void)
    {
        decisionHandler(.allow);
    }
    
}
