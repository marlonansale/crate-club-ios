//
//  ProductViewController.swift
//  CrateClubNew
//
//  Created by MacPro on 28/01/2019.
//  Copyright © 2019 MacPro. All rights reserved.
//

import UIKit
import MarqueeLabel
import Material

class ProductViewController: RootDeviceCheckerViewController {

    let arrMenu = ["PREMIUM","OPERATOR","PRO", "STANDARD","AIR DROP"]
    var arrArticle = [ArticleModel]()
    var page = 0
    var selectedMenu = -1
    
    var isCheckPremium = false
    var isCheckOperator = false
    var isCheckPro = false
    var isCheckStandard = false
    
    var isLoaded = false
    
    @IBOutlet weak var viewSnack: UIView!
    @IBOutlet weak var lblSurvival: MarqueeLabel!
    @IBOutlet weak var collectionViewProduct: UICollectionView!
    @IBOutlet weak var collectionViewType: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionViewProduct.register(UINib.init(nibName: "KnowledgeCollectionCell", bundle: nil), forCellWithReuseIdentifier: "KnowledgeCollectionCell")
        collectionViewType.register(UINib.init(nibName: "ShipProdPhoneCell", bundle: nil), forCellWithReuseIdentifier: "ShipProdPhoneCell")
       
        getKnowledge()
    }
    
    func getKnowledge(){
        self.page =  self.page + 1
        KnowledgeManager.shared.getKnowledge(page: self.page, complete: { (complete) in
            DispatchQueue.main.async {
                self.arrArticle = complete as! [ArticleModel]
                
                self.collectionViewProduct.reloadData()
            }
        }) { (fail) in
            DispatchQueue.main.async {
                self.alertError(error: fail)
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func onCloseTips(_ sender: Any) {
        viewSnack.isHidden = true
    }
    
}

extension ProductViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    // MARK: - Collection View
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewType {
            return arrMenu.count
        }else{
            return arrArticle.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        if collectionView == collectionViewType {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShipProdPhoneCell", for: indexPath) as! ShipProdPhoneCell
        
            cell.lblName.text = arrMenu[indexPath.row]
            cell.viewBorderRight.isHidden = arrMenu.count == indexPath.row + 1
        
        
        
            if indexPath.row == self.selectedMenu {
                cell.contentView.backgroundColor = UIColor.colorActive();
            }else{
                cell.contentView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            }
        
            return cell
            
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KnowledgeCollectionCell", for: indexPath) as! KnowledgeCollectionCell
    
            let article = self.arrArticle[indexPath.row]
            cell.setData(article: article)
    
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionViewType {
            return CGSize(width: self.view.frame.width / CGFloat(arrMenu.count) , height: collectionView.frame.height );
        }else{
            return CGSize(width: collectionView.frame.width , height: KnowledgeCellHeight );
            
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if self.collectionViewType == collectionView{
            selectedMenu = indexPath.row
            self.collectionViewType.reloadData()
            
        }else{
            if selectedMenu == -1 {
                let article =   self.arrArticle[indexPath.row]
                
                let vc = UIStoryboard.init(name: "Product", bundle: nil).instantiateViewController(withIdentifier: "KnowledgeViewController") as! KnowledgeViewController
                vc.article = article
                
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
        }
    }

}
