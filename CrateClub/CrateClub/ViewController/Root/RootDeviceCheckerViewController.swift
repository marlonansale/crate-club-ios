//
//  RootDeviceCheckerViewController.swift
//  CrateClubNew
//
//  Created by MacPro on 25/01/2019.
//  Copyright © 2019 MacPro. All rights reserved.
//

import UIKit

class RootDeviceCheckerViewController: RootViewController {

    @IBOutlet weak var constantTableLeft: NSLayoutConstraint?
    @IBOutlet weak var constantTableRight: NSLayoutConstraint?
    @IBOutlet weak var constantBottom: NSLayoutConstraint?
    @IBOutlet weak var imgViewFooter: UIImageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if constantBottom != nil, constantTableLeft != nil, constantTableRight != nil{
            
            if UIDevice.current.userInterfaceIdiom == .pad {
                constantTableLeft!.constant = 30
                constantTableRight!.constant = 30
                constantBottom!.constant = 30
            }else{
                constantBottom!.constant = 16
            }
        }
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
