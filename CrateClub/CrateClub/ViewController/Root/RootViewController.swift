//
//  RootViewController.swift
//  CrateClub
//
//  Created by MacPro on 09/01/2019.
//  Copyright © 2019 MacPro. All rights reserved.
//

import UIKit
import SystemConfiguration

class RootViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.navigationController != nil {
            let image = UIImage.init(named: "nav_logo");
            let imageView = UIImageView.init(frame:
                CGRect.init(x: 0, y: 0, width: 40, height: 30))
            imageView.image = image
            imageView.contentMode = .scaleAspectFit
            imageView.clipsToBounds = true
            
            imageView.backgroundColor = UIColor.clear
            
            self.navigationItem.titleView = imageView
            
            self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
      
        

        // Do any additional setup after loading the view.
    }
    
    func animateTextField(textField: UITextField, up: Bool)
    {
        if UIDevice.current.userInterfaceIdiom != .pad {
            let movementDistance:CGFloat = -100
            let movementDuration: Double = 0.3
            
            var movement:CGFloat = 0
            if up
            {
                movement = movementDistance
            }
            else
            {
                movement = -movementDistance
            }
            UIView.beginAnimations("animateTextField", context: nil)
            UIView.setAnimationBeginsFromCurrentState(true)
            UIView.setAnimationDuration(movementDuration)
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
            UIView.commitAnimations()
        }
    }

    func alertError(error:String)  {
        
    }
    
    
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
