//
//  SplashViewController.swift
//  CrateClub
//
//  Created by MacPro on 08/01/2019.
//  Copyright © 2019 MacPro. All rights reserved.
//

import UIKit

class SplashViewController: RootViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let vc =   UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NavGuest") as! NavigationViewController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.changeRoot(vc: vc)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
